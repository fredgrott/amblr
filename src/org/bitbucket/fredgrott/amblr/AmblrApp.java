package org.bitbucket.fredgrott.amblr;

import org.bitbucket.fredgrott.androidsandwich.app.*;

import android.content.SharedPreferences;

/**
 * The basic setup of AbmlrApp class. Because we are
 * using the DefaultAppplication class from the AndroidSandwich
 * APL we only need to over-ride two methods.
 * 
 * @author fredgrott
 *
 */
public class AmblrApp extends DefaultApplication {
	String appLogTag = "AmblrAppLog";
	
	@Override
	public void setUpSharedPrefs() {
		
		
		SharedPreferences appPrefs = (SharedPreferences) new AmblrSharedPrefs(this.getApplicationContext());
		super.setUpSharedPrefs();
	}
	
	@Override
	public void setUpCaches() {
		// TODO Auto-generated method stub
		super.setUpCaches();
	}

}
