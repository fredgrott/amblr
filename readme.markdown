Amblr
=====

A Tumblr Android Proof of Concept Application.
WARNING: Its alpha, I am not responsible for it breaking
your android device.

Testing
=======

Unit and Instrumented testing is using the robotium/android-mock combination with
a mix of roboguice.

Functional Testing via  Chimpchat(monkeyrunner replacement). And we use cucumber-jvm.

The idea here is not to get to far away from using java to test as when you introduce 
another langue yu get more opportunties for things to break as you than have to keep
up two computer langus and their environments, etc.

Project Structure
=================

project/
      bin/
      gen/
      src/
      assets/
      teslib/ for our cucumber testing
      res/
      ressrc/  res digital files inputed to res, they get stored here as a way to
               have a copy incase extra mdofications needed and because we can 
               do an albumshaper catalogue
      

Project Dependencies
====================

Android sdk jars ofcoures, the compatibility library, and these projects:

AmblrTest Instrumented testing
AmblrCucumberTest Cucumber testing
AmblrJavaMonkey   Chimpchat(MonkeyRunner Replacement) Java Monkey testing using Sikuli

